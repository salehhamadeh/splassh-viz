Meteor.startup(function () {
  if (DataPoints.find().count() === 0) {
    DataPoints.insert({description: "Dead pig found at coast", lat: 33.53224, lon: -84.2770});
    DataPoints.insert({description: "Water acidity is too high", lat: 33.53224, lon: -80.2770});
    DataPoints.insert({description: "Saleh's seafood restaurant.", lat: 32.53224, lon: -84.2770});
  }
});