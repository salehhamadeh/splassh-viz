// TODO:
// *drawLMap should NOT reset orientation and center map. DONE
// *new method to center map
// *remove highlight from being executed when the database updates
// *refactor code to keep map functions away from Template handling
var lmap = null;
var svgMap = null;
var gMap = null;
var centroids, bounds;

//------Used for left map--------
var width = 570, height = 350;
var projection = null;

var svg = null;

var statePaths = null; 
var lakePaths = null; 
var bubble = null;
//-------------------------------

Template.map.rendered = function() {
  //var data, schools,centroids,bounds, lbounds;
  var data, schools, lbounds;
  projection = d3.geo.albersUsa()
    .scale(650)
    .translate([width / 2, height / 2]);
  var path = d3.geo.path()
    .projection(projection);

  svg = d3.select("#map").append("svg")
    .attr("width", width)
    .attr("height", height);

  statePaths = svg.append("g")
    .attr("id","statePath"); 
  lakePaths = svg.append("g")
    .attr("id","lakePath"); 
  bubble = svg.append("g")
    .attr("id", "bubbles");

  var canvas = d3.select("#map").insert("canvas")
    .attr("width", width)
    .attr("height", height)
    .style("opacity", 1);

  var context = canvas.node().getContext("2d");
  context.fillStyle = "#80cafa";
  context.fillRect(0, 0, width, height);

  queue()
    .defer(d3.json, "data/state.topojson")
    .defer(d3.json, "data/state_centroid.geojson")
     .defer(d3.json, "data/greatlakes.geojson")
    .await(ready);

  function ready(error,state,stateCentroid,greatlakes){
    var states = topojson.feature(state, state.objects.state).features.filter(function(d){
      return d.id != '60' && d.id != '69' && d.id != '66' && d.id != '78'});
    
    bounds = d3.geo.bounds({type:"FeatureCollection", features:states});
    centroids = stateCentroid.features.filter(function(d){
      return d.properties.fips != '60' && d.properties.fips != '69' && d.properties.fips != '66' && d.properties.fips != '78'});
    centroids.forEach(function(d){d.fips = parseInt(d.properties.fips,10)});

    var s = statePaths.selectAll("path")
      .data(states)
        .enter()
        .append("path")
        .attr("d",path)
        .attr("class", "state");
    var l = lakePaths.selectAll("path")
      .data(greatlakes.features)
        .enter()
        .append("path")
        .attr("d",path)
        .attr("class", "lakes");
  }
};

Template.detailmap.rendered = function() {
  //leaflet map
  lmap = new L.Map('detailmap', {
    center: new L.LatLng(39.5, -98.5),
    zoom: 4,
    layers: new L.TileLayer('http://a.tiles.mapbox.com/v3/herwig.map-onzoztaf/{z}/{x}/{y}.png')
    // layers: new L.TileLayer('http://' + ["a", "b", "c", "d"][Math.random() * 4 | 0] +'.tiles.mapbox.com/v3/fcc.map-qly54czi/{z}/{x}/{y}.png')
    //layers: new L.TileLayer('http://' + ["a", "b", "c", "d"][Math.random() * 4 | 0] +'.tiles.mapbox.com/v3/fcc.map-toolde8w/{z}/{x}/{y}.png')
  });

  svgMap = d3.select(lmap.getPanes().overlayPane).append("svg");
  gMap = svgMap.append("g").attr("class", "leaflet-zoom-hide");
};

Template.toolbar.events({
  'click #type': function(event) {
    //drawLMap([{lat: 33.53224, lon: -84.2770}, {lat: 33.53224, lon: -80.2770}, {lat: 32.53224, lon: -84.2770}]);
    var cur = DataPoints.find({})
    cur.observe({
      added: function(d, i, b) {
        var pts = DataPoints.find({}).fetch();
        reset();
        drawLMap(pts);
        fitMapToPoints(pts);
        highlight(pts[0]);
        drawTable(pts);
        console.log(d);
      },
      changed: function(n, o) {
        var pts = DataPoints.find({}).fetch();
        reset();
        drawLMap(pts);
        fitMapToPoints(pts);
        highlight(pts[0]);
        drawTable(pts);
        console.log(d);
      },
      removed: function(o) {
        var pts = DataPoints.find({}).fetch();
        reset();
        drawLMap(pts);
        fitMapToPoints(pts);
        highlight(pts[0]);
        drawTable(pts);
        console.log(d);
      }
    });
  }
});

//Points is an array of dictionaries containing lat and lon. Ex: [{lat: 1, lon: 2}]
function drawLMap(pts){
  var coords = [];
  pts.forEach(function(d){
    var a = [+d.lon, +d.lat];
    coords.push(a);
  })

  //Plot datapoints on left (static svg) map
  //enter
  bubble.selectAll(".lcircle").data(coords)
    .enter().append("circle")
    .attr("class", "lcircle")
    .attr("transform", function(d) {
      var centroid = projection(d)
      x = centroid[0];
      y = centroid[1];
      return "translate(" + x + "," + y + ")";
    })
    .attr("r", 2);

  //Plot datapoints on the right (leaflet) map
  bubblesL = gMap.selectAll(".lcircle")
    .data(coords, function(d){return d});
  //enter
  bubblesL.enter().append("circle").attr("class","lcircle")
    .attr("transform", function(d){
      var c = project(d);
      var x = c[0], y = c[1];
      return "translate(" + x + "," + y + ")";
    })
    .attr("r", 3)
    //.on("click", function(d){showState(d,this)});
  //update
  bubblesL.attr("class","lcircle")
    .transition()
      .duration(750)
      .attr("transform", function(d){
            var c = project(d);
            var x = c[0], y = c[1];
            return "translate(" + x + "," + y + ")";
          })
      .attr("r", 3)
  //exit
  bubblesL.exit().attr("class","lcircle")
    .transition()
      .duration(750)
      .style("fill-opacity",1e-6)
      .remove();

  lmap.on("viewreset", resetMap);
  resetMap();
}

//Center and zoom map around an array of points
function fitMapToPoints(pts) {
  var latlon=[];
  pts.forEach(function(d){
    var pt = new L.LatLng(+d.lat,+d.lon);
    latlon.push(pt);
  });

  if(pts.length > 1) {
    lbounds= new L.LatLngBounds(latlon);
    lmap.fitBounds(lbounds);
  }
  else if(pts.length == 1) {
    lmap.setView(latlon[0], 10);
  }
}

//Removes all points and pulses from map
function reset(){
  d3.selectAll('.circle').remove();
  d3.selectAll('.lcircle').remove();
       // d3.selectAll('.circleKey').remove();
  d3.selectAll(".pulse_circle").remove();
       // d3.select("#contents").html("");
}

function resetMap(){
  // var bottomLeft = project([bounds.getSouthWest().lng, bounds.getSouthWest().lat]),
  //      topRight = project([bounds.getNorthEast().lng, bounds.getNorthEast().lat]);
  var bottomLeft = project(bounds[0]),
       topRight = project(bounds[1]);

  svgMap.attr("width", topRight[0] - bottomLeft[0])
    .attr("height", bottomLeft[1] - topRight[1])
    .style("margin-left", bottomLeft[0] + "px")
    .style("margin-top", topRight[1] + "px");

  gMap.attr("transform", "translate(" + -bottomLeft[0] + "," + -topRight[1] + ")");
  d3.transition(bubblesL)
    .attr("class", "lcircle")
    .attr("transform", function(d) {
      var c = project(d);
      x = c[0];
      y = c[1];
      return "translate(" + x + "," + y + ")";
    })
   
  gMap.selectAll(".pulse_circle")
    .attr("class", "pulse_circle")
    .attr("transform", function(d) {
      var centroid = project(d);
      x = centroid[0];
      y = centroid[1];
      return "translate(" + x + "," + y + ")";
    })
}

// Use Leaflet to implement a D3 geographic projection.
function project(x) {
  var point = lmap.latLngToLayerPoint(new L.LatLng(x[1], x[0]));
  return [point.x, point.y];
}

function pulse() {
  return function(d, i, j) {
    //the stuff before transition() resets the
    //attributes of the pulser when this function is
    //called again
    d3.select(this).attr("r", 5).style("stroke-opacity", 1.0)
      .transition()
      .ease("linear") //appears a lot more smoother
      .duration(1000)
      .attr("r",25)
      .style("stroke-opacity", 0.0)
      .each("end", pulse()); //lather rinse repeat
  };  
}

//Draws a pulse animation around a point. pt is a dictionary with a lat and lon
function highlight(pt){
  var coord=[pt.lon, pt.lat];

  d3.selectAll(".pulse_circle").remove();
  
  //Put pulse on left (static svg) map
  bubble.selectAll(".pulse_circle").data([coord])
    .enter().append("circle")
    .attr("class", "pulse_circle")
    .attr("transform", function(d) {
      var centroid = projection(d)
      x = centroid[0];
      y = centroid[1];
      return "translate(" + x + "," + y + ")";
    })
    .each(pulse());

  //Put pulse on right (leaflet) map
  gMap.selectAll(".pulse_circle").data([coord])
    .enter().append("circle")
    .attr("class", "pulse_circle")
    .attr("transform", function(d) {
      var centroid = project(d)
      x = centroid[0];
      y = centroid[1];
      return "translate(" + x + "," + y + ")";
    })
    .each(pulse());
}

function drawTable(dataPoints){
  d3.select("#contents").html("");

  var content = "", d=dataPoints;
  content += "<table id='tbl-recordDetails' class='table table-hover tablesorter'><thead><tr><th><div class='sort-wrapper'>Description &nbsp;<span class='sort'></span></div></th></tr></thead>";

  d3.select("#txtSelection").html("Top " + d.length + " data points");

  content += "<tbody>";
  for (var i = 0, length = d.length; i<length; i++) {
  //console.log(d[i].fips + " " + getStateName(d[i].fips).abbrName)
    content += "<tr data-lat=" + d[i].lat + " data-lon=" + d[i].lon + "><td>" + d[i].description + "</td></tr>";
  } 
  content +="</tbody></table>";

  $("#contents").html(content);
  //TODO: Add the sort functionality at thead initTblSort();

  d3.selectAll("#tbl-recordDetails tbody tr").on("click", function(){
    var tr = d3.select(this);
    var pt = {lat: tr.attr("data-lat"), lon: tr.attr("data-lon")}
    d3.selectAll("#tbl-recordDetails tbody tr").classed('tablerowselected',false);
    highlight(pt);
    fitMapToPoints([pt]);
  });
}